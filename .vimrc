" """""""""""""""""""""""""""""
" ===== General Settings  =====

" Automatically expand tabs into spaces
set expandtab

" Tabs are four spaces
set shiftwidth=4
set softtabstop=4
set tabstop=4

" Use expected backspace behavior
set backspace=indent,eol,start

" Turn off search highlighting
set nohls

" Turn of swap files and backups
set nobackup
set noswapfile
set nowritebackup

" Turn off text wrapping
set nowrap

" Display position coordinates in the bottom right
set ruler

" Abbreviate messages and disable intro screen
set shortmess=atI

" Get rid of omnicomplete doc preview
set completeopt=menu

" Use filetype-specific plugins and indention
set nosmartindent
filetype plugin indent on

" Turn off automatic line breaking in html and css
au BufRead, BufNewFile *.html, *.css set textwidth=0

" Ignore compiled Python
set wildignore+=*.pyc

" """""""""""""""""""""""""""""""
" ===== Custom Key Bindings =====


" """""""""""""""""""""""""""""""
" ======= Plugin Settings =======

" Use pathogen for plugins

execute pathogen#infect()

" """""""""""""""""""""""""""""""
" ======= Theme Settings ========

syntax enable
set background=dark
colorscheme solarized
